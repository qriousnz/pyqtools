# -*- coding: utf-8 -*-
"""
Module for reading in YAML config files.

Requirements:
- `pyaml` module
"""

# Created: 2017-01-24 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# This file is part of a Python tools collection for Qrious.
#
# You should have received a copy of the license along with this
# program.
#
# Copyright 2017 by Qrious Limited, Auckland, New Zealand
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import yaml

GLOBAL_VAULT_SECRETS_FILE = '/etc/vault.yml'
USER_VAULT_SECRETS_FILE = os.path.join(os.environ['HOME'], '.vault.yml')

def get_configuration():
    """
    Reads the connection/encryption configuration and returns it.

    :return: Dictionary containing the configuration elements.
    """

    try_files = [USER_VAULT_SECRETS_FILE, GLOBAL_VAULT_SECRETS_FILE]

    for config_file in try_files:
        try:
            config = yaml.load(open(config_file, 'rt').read())
        except yaml.YAMLError as e:
            logging.debug('Config file {} not found/accessible: {}'
                          .format(config_file, str(e)))
        if config:
            break
    if not config:
        logging.error('Could not access a suitable config file.')
        sys.exit(1)

    return config

config = _get_configuration()
