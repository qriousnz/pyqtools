# -*- coding: utf-8 -*-
"""
Revera Vault access module for uploading, downloading, deleting and listing
files.

Requirements:
- `requests` or `pycurl` library
  (`pycurl` is best installed from distribution packages)
- `lxml` module
"""

# Created: 2016-09-22 Guy K. Kloss <guy.kloss@qrious.co.nz>
#
# This file is part of a Python tools collection for Qrious.
#
# You should have received a copy of the license along with this
# program.
#
# Copyright 2016-2017 by Qrious Limited, Auckland, New Zealand
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import base64
import datetime
import gzip
import hashlib
import hmac
import io
import logging
from lxml import etree
import os
import pprint
import re
import sys

from pyqtools import config


# Try to import the two options for the HTTP request library options used in
# this code. The code will fail later if the one (auto) selected is missing.
try:
    import requests
except ImportError:
    pass
try:
    import pycurl
except ImportError:
    pass

BASE_URL = 'https://vault.revera.co.nz/'
LOG_FILE = '/var/log/vault.log'
_DATE_FORMAT = '%a, %d %b %Y %X +0000'

_AWS_NS_EXPRESSION = re.compile('^(<?[^>]+?>\s?)(<\w+)'
                                ' xmlns=[\'"](http://[^\'"]+)[\'"](.*)',
                                re.MULTILINE)




def _http_execute_requests(method, url, data=None, headers=None, proxy=None):
    """
    Execute an HTTP request using the Python requests module.

    :param method: HTTP verb for execution. One of 'GET', 'PUT', 'DELETE', ...
    :param url: URL for the request.
    :param data: Optional data to pass in the request's body (default: None).
        This can be a dictionary, string or readable stream (should provide a
        byte stream, not a unicode string stream).
    :param headers: Optional dictionary with header entries as key-value pairs
        (default: None).
    :param proxy: Optionally proxy to use (default: None).
    :return: Tuple (byte_stream, http_status):
        (file-like object to read from, int)
    """
    proxies = {'https': proxy} if proxy else None

    try:
        if method == 'GET':
            request = requests.get(url, headers=headers,
                                   proxies=proxies, stream=True)
        elif method == 'PUT':
            request = requests.put(url, data=data, headers=headers,
                                   proxies=proxies, stream=True)
        elif method == 'DELETE':
            request = requests.delete(url, headers=headers,
                                      proxies=proxies, stream=True)
        else:
            raise ValueError('Unsupported HTTP method: {}'.format(method))
        content = request.raw
        if (('Content-Encoding' in request.headers) and
                (request.headers['Content-Encoding'] == 'gzip')):
            content = gzip.GzipFile(fileobj=request.raw)
            logging.debug('HTTP {} response status for {} {}.'
                          .format(request.status_code, method, url))
        return content, request.status_code
    except requests.exceptions.RequestException as e:
        logging.exception('HTTP {} request FAILED with URL {}: {}'
                          .format(method, url, e.output))
        sys.exit(1)


def _http_execute_curl(method, url, data=None, headers=None, proxy=None):
    """
    Execute an HTTP request using cURL.

    :param method: HTTP verb for execution. One of 'GET', 'PUT', 'DELETE', ...
    :param url: URL for the request.
    :param data: Optional data to pass in the request's body (default: None).
        This can be a dictionary, string or readable stream (should provide a
        byte stream, not a unicode string stream).
    :param headers: Optional dictionary with header entries as key-value pairs
        (default: None).
    :param proxy: Optionally proxy to use (default: None).
    :return: Tuple (byte_stream, http_status):
        (file-like object to read from, int)
    """
    connection = pycurl.Curl()
    if proxy:
        connection.setopt(pycurl.PROXY, proxy)
    if method == 'GET':
        connection.setopt(pycurl.GET, 1)
    elif method == 'PUT':
        connection.setopt(pycurl.PUT, 1)
        connection.setopt(pycurl.READFUNCTION, data.read)
    elif method == 'DELETE':
        connection.setopt(pycurl.DELETE, 1)
    else:
        raise ValueError('Currently unsupported HTTP method: {}'
                         .format(method))
    header_lines = ['{}: {}'.format(k, v) for k, v in headers.items()]
    connection.setopt(pycurl.HTTPHEADER, header_lines)
    response_body = io.BytesIO()
    connection.setopt(pycurl.WRITEFUNCTION, response_body.write)
    connection.setopt(pycurl.URL, url)
    logging.debug('HTTP {} response status for {} {}.'
                  .format(connection.getinfo(pycurl.HTTP_CODE), method, url))
    response_body.seek(0)
    return response_body, connection.getinfo(pycurl.HTTP_CODE)


def _strip_namespace(xml):
    """
    Remove the top-level AWS namespace:

    <ListBucketResult xmlns="http://s3.amazonaws.com/doc/2006-03-01/">

    :param: XML content as string.
    :return: Tuple of stripped XML and XML namespace as strings.
    """
    occurrence = _AWS_NS_EXPRESSION.match(xml)
    if occurrence:
        xmlns = occurrence.groups()[2]
        xml = _AWS_NS_EXPRESSION.sub(r'\1\2\4', xml)
    else:
        xmlns = None
    return xml, xmlns


def _tree_from_xml(xml):
    xml, xmlns = _strip_namespace(xml)
    try:
        tree = etree.ElementTree.fromstring(xml)
        if xmlns:
            tree.attrib['xmlns'] = xmlns
        return tree
    except xml.etree.ElementTree.ParseError as e:
        logging.exception('Error parsing XML: %s', e)
        sys.exit(1)


def _text_from_xml(xml, xpath):
    tree = _tree_from_xml(xml)
    result = None
    if tree.tag.endswith(xpath):
        if tree.text is not None:
            result = _decode_from_s3(tree.text)
    else:
        result = tree.findtext(xpath)
        if result is not None:
            result = _decode_from_s3(result)
    return result


def _list_from_xml(xml, node):
    tree = _tree_from_xml(xml)
    nodes = tree.findall('.//{}'.format(node))
    return _parse_nodes(nodes)


def _decode_from_s3(a_string, errors='replace'):
    """
    Convert S3 UTF-8 'string' to Unicode or raise an exception.
    """
    if type(a_string) == unicode:
        return a_string
    return unicode(a_string, 'UTF-8', errors)


def _parse_nodes(nodes):
    # WARNING: Ignores text nodes from mixed xml/text.
    # For instance <tag1>some text<tag2>other text</tag2></tag1>
    # will ignore "some text" node.
    result = []
    for node in nodes:
        retval_item = {}
        for child in node.getchildren():
            name = _decode_from_s3(child.tag)
            if child.getchildren():
                retval_item[name] = _parse_nodes([child])
            else:
                found_text = node.findtext('.//{}'.format(child.tag))
                retval_item[name] = None
                if found_text is not None:
                    retval_item[name] = _decode_from_s3(found_text)
        result.append(retval_item)
    return result


class Vault(object):
    """
    Access tool set for Revera Vault storage.
    """

    base_url = None
    """Base URL for Vault."""

    config = None
    """Vault configuration containing the required secrets (keys)."""

    proxy = None
    """HTTPS proxy to use for the connection."""

    use_curl = False
    """If true, use cURL library instead of requests."""

    def __init__(self, use_curl=None, config=None):
        """
        Constructor.

        :param use_curl: Optional flag to use cURL rather than the Python
            requests library (default: False). If the `requests` is not
            present, the default will be to use cURL.
        :param config: Optional configuration object (dictionary) with all
            required access secrets (default: None). If not given, it will
            attept to read them from default locations via the `config` module.
        """
        self.base_url = BASE_URL
        if config:
            self.config = config
        else:
            self.config = config.get_configuration()
        self.proxy = None
        if ('proxy' in self.config) and ('https' in self.config['proxy']):
            self.proxy = self.config['proxy']['https']
        self._secret_key = self.config['vault']['secret_key']
        self._access_key = self.config['vault']['access_key']
        if use_curl is not None:
            self._use_curl = use_curl
        else:
            # If we don't have requests, switch to use pycurl.
            if 'requests' not in globals():
                Vault.use_curl = True

    def _http_execute(self, method, url, data=None, headers=None):
        """
        Execute an HTTP request.

        :param method: HTTP verb for execution. One of 'GET', 'PUT',
            'DELETE', ...
        :param url: URL for the request.
        :param Data: Optional data to pass in the request's body
            (default: None). This can be a dictionary, string or readable
            stream (should provide a byte stream, not a unicode string stream).
        :param headers: Optional dictionary with header entries as key-value
            pairs (default: None).
        :return: Tuple (byte_stream, http_status):
            (file-like object to read from, int)
        """
        if self._use_curl:
            return _http_execute_curl(method, url, data,
                                      headers, self.proxy)
        else:
            return _http_execute_requests(method, url, data,
                                          headers, self.proxy)

    def _make_headers(self, method, checksum='', content_type='', path='/'):
        """
        Makes an initial set of header fields required for the request message
        authorisation.

        :param method: HTTP verb for execution. One of 'GET', 'PUT',
            'DELETE', ...
        :param checksum: Optional checksum of content to transfer, e.g. on a
            PUT (default: empty string). MD5 hash of content in base64
            (standard) encoded binary form.
        :param content_type: Content type of data to be transferred in the
            request body.
        :param path: Path of the bucket or file in bucket on Vault.
        """
        date = datetime.datetime.utcnow().strftime(_DATE_FORMAT)
        fields = {
            'method': method,
            'checksum': checksum,
            'content_type': content_type,
            'date': date,
            'path': path
        }
        message = ('{method}\n'
                   '{checksum}\n'
                   '{content_type}\n'
                   '{date}\n'
                   '{path}\n'
                   .format(fields))
        signature = hmac.HMAC(self._secret_key.encode(),
                              msg=message.encode(),
                              digestmod=hashlib.sha1).digest()
        headers = {
            'Date': date,
            'Authorization': 'AWS {}:{}'.format(self._access_key, signature)
        }
        return headers

    def _file_put_request(self, file_stream, path):
        """
        HTTP PUT request for the file upload.

        :param file_stream: File-like object to read from for upload. The
            object must also be "seekable", as after checksumming it, the
            stream position must be reset to the start again.
        :param path: Absolute destination path to the file including bucket
            name.
        :return: Tuple (byte_stream, http_status):
            (file-like object to read from, int)
        """
        data_content = file_stream.read()
        file_checksum = base64.standard_b64encode(
            hashlib.md5(data_content).digest())
        file_stream.seek(0)
        headers = self._make_headers('PUT', path=path, checksum=file_checksum,
                                     content_type='application/octet-stream')
        full_url = '/'.join([self.base_url, path])
        return self._http_execute('PUT', full_url,
                                  data=file_stream, headers=headers)

    def _file_get_request(self, path):
        """
        HTTP GET request for the file content.

        :param path: Path to the file including bucket name.
        :return: Tuple (byte_stream, http_status):
            (file-like object to read from, int)
        """
        headers = self._make_headers('GET', path=path)
        full_url = '/'.join([self.base_url, path])
        return self._http_execute('GET', full_url, headers=headers)

    def _file_delete_request(self, path):
        """
        HTTP DELETE request for removing a file.

        :param path: Path to the file including bucket name.
        :return: Tuple (byte_stream, http_status):
            (file-like object to read from, int)
        """
        headers = self._make_headers('DELETE', path=path)
        full_url = '/'.join([self.base_url, path])
        return self._http_execute('DELETE', full_url, headers=headers)

    def _list_request(self, bucket='/'):
        """
        HTTP request for content list in a bucket.

        :param bucket: Name of the bucket (default `/` for all contents).
        :return: Tuple (byte_stream, http_status):
            (file-like object to read from, int)
        """
        headers = self._make_headers('GET', path=bucket)
        full_url = '/'.join([self.base_url, bucket])
        return self._http_execute('GET', full_url, headers=headers)

    def list_all_buckets(self):
        """
        Lists all buckets.

        :return: List of dicts for buckets with their meta-data.
        """
        response_data, status = self._list_request()
        root = etree.fromstring(response_data.read())
        root_xml = etree.tostring(root,
                                  encoding='UTF-8',
                                  xml_declaration=True)
        bucket_list = _list_from_xml(root_xml, 'Bucket')
        logging.debug('HTTP GET request status for list on bucket "/": {}'
                      .format(status))
        logging.debug('Bucket list: {}'
                      .format(pprint.pformat(bucket_list)))
        return bucket_list

    def list_bucket_info(self, bucket):
        """
        XXX

        :return: List of entries.
        """
        result = []
        completed = False
        while not completed:
            response_data, status = self._list_request(bucket)
            root = etree.fromstring(response_data.read())
            root_xml = etree.tostring(root,
                                      encoding='UTF-8',
                                      xml_declaration=True)
            current_list = _list_from_xml(root_xml, 'Contents')
            result.extend(current_list)
            current_prefixes = _list_from_xml(root_xml, 'CommonPrefixes')
            is_truncated = _text_from_xml(root_xml, './/IsTruncated')
            completed = (is_truncated is None)
            logging.debug('HTTP GET request status for list on bucket "{}": {}'
                          .format(bucket, status))
            logging.debug('Current list: {} current_prefixes: {} truncated: {}'
                          .format(current_list, current_prefixes,
                                  is_truncated))
        return result

    def put_file(self, file_stream, path):
        """
        Uploads a file to a bucket.

        :param file_stream: File-like object to read from for upload.
        :param path: Absolute destination path to the file including bucket
            name.
        :return: None
        """
        _, status = self._file_put_request(file_stream, path)
        logging.debug('Uploaded file {}, HTTP status: {}'.format(path, status))

    def get_file(self, path):
        """
        Retrieves a file with a given path from a bucket.

        :param path: Full file path including the bucket as a string.
        :return: File-like object.
        """
        response_data, status = self._file_get_request(path)
        logging.debug('Got file {}, HTTP status: {}'.format(path, status))
        return response_data

    def delete_file(self, path):
        """
        Deletes a file with a given path from a bucket.

        :param path: Full file path including the bucket as a string.
        :return: None
        """
        _, status = self._file_get_request(path)
        logging.debug('Deleted file {}, HTTP status: {}'.format(path, status))
