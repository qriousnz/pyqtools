# Python Tools Collection for Qrious

A collection of tools frequently needed for various tasks, like
accessing Revera Vault, en/decrypting using GnuPG, etc.


## Prerequisites

- Python 2.7 or 3.x
- `pyaml` module
- `requests` or `pycurl` library
  (`pycurl` is best installed from distribution packages)


## Install

If a copy exists (e.g. via ``git clone``) in a directory, do the following:

    pip install pyqtools/

Or to upgrade:

    pip install --upgrade pyqtools/

If this should fail to produce the installed library usable for all
users. If executed by `root` it is possible that the ``umask`` is set
too restrictively (e.g. as on CentOS with a restrictive setup like
SELinux), just set the ``umask`` first:

    umask 0022
    pip install pyqtools/


## Run

...


## Setting up a virtual environment

Set up and activate for Python 2:

    virtualenv env2 --no-site-packages -p /usr/bin/python2
    source env2/bin/activate

Set up and activate for Python 3:

    virtualenv env3 --no-site-packages -p /usr/bin/python3
    source env3/bin/activate

Install required packages:

    pip install -r requirements.txt


## Licence

Copyright 2016 by Qrious Limited, Auckland, New Zealand

Licenced under the Apache Licence, Version 2.0 (the "Licence");
you may not use this file except in compliance with the Licence.
You may obtain a copy of the Licence at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the Licence is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the Licence for the specific language governing permissions and
limitations under the Licence.
