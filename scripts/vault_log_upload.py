#!/usr/bin/env python
# encoding: utf-8
'''
Script to upload log archives to vault, that are symmetrically encrypted (GnuPG).

@author:     Guy Kloss
@copyright:  2017 Qrious Ltd. All rights reserved.
@license:    Apache License 2.0
@contact:    guy.kloss@qrious.co.nz
'''

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
import datetime
import os
import sys
import tempfile
import gnupg
from pyqtools import vault
from pyqtools.config import config

__all__ = []
__version__ = 0.1
__date__ = '2017-01-23'
__updated__ = '2017-01-23'

DEBUG = True
TESTRUN = False
PROFILE = False

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''

    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = 'E: {}'.format(msg)

    def __str__(self):
        return self.msg

    def __unicode__(self):
        return self.msg


def log_upload(source_file, catalog):
    """
    Encrypt and upload a log file to Revera Vault.

    :param source_file: Name of the source file as a string.
    :param catalog: Name of the Vault destination catalog (including bucket
        name) as a string.
    """
    filename_abs = os.path.abspath(source_file)
    # Encrypt the file.
    gpg = gnupg.GPG()
    with open(filename_abs, 'rb') as instream:
        _, encrypted_file = tempfile.mkstemp()
        encrypted_data = gpg.encrypt_file(instream, recipients=None,
                                          output=encrypted_file,
                                          symmetric=config['gpg']['algorithm'],
                                          passphrase=config['gpg']['key'])
        logging.info('Encrypted file for {} OK: {}'
                     .format(source_file, encrypted_data.ok))

    # Upload the file to Vault.
    filename_gpg = os.path.basename('{}.gpg'.format(source_file))
    my_vault = vault.Vault()
    with open(encrypted_file, 'rb') as instream:
        yesterday = datetime.datetime.today() - datetime.timedelta(1)
        destination_path = '/'.join(catalog,
                                    yesterday.strftime('%Y/%m/%d'),
                                    filename_gpg)
        logging.info('Upload file path: {}'.format(destination_path))
        my_vault.put_file(instream, destination_path)
        logging.info('Successfully uploaded file {} to {}.'
                     .format(source_file, destination_path))

    os.remove(encrypted_file)


def main(argv=None):  # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = 'v{}'.format(__version__)
    program_build_date = str(__updated__)
    program_version_message = '%(prog)s {} ({})'.format(program_version,
                                                        program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split('\n')[1]
    program_license = '''%s

  Created by Guy Kloss on %s.
  Copyright 2017 Qrious Ltd. All rights reserved.

  Licensed under the Apache License 2.0
  http://www.apache.org/licenses/LICENSE-2.0

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser.
        parser = ArgumentParser(description=program_license,
                                formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument('-v', '--verbose', dest='verbose', action='count',
                            help='set verbosity level [default: %(default)s]')
        parser.add_argument('-V', '--version',
                            action='version', version=program_version_message)
        parser.add_argument('-b', '--bucket', type=str,
                            help='a bucket name, e.g. msd-staging-log')
        parser.add_argument('-c', '--catalog', type=str,
                            help='a catalog name, e.g. logstash')
        parser.add_argument(dest='files', metavar='files', type=str, nargs='+',
                            help='paths to file(s) to upload [default: %(default)s]')

        # Process arguments.
        args = parser.parse_args()

        bucket = args.bucket
        catalog = args.catalog
        files = args.files
        verbose = args.verbose

        if verbose > 0:
            logging.getLogger().setLevel(logging.DEBUG)
            print('Verbose mode on.')
        for item in files:
            log_upload(item, '/'.join(bucket, catalog))
        return 0
    except KeyboardInterrupt:
        # Handle keyboard interrupt.
        return 0
    except Exception as e:
        if DEBUG or TESTRUN:
            raise(e)
        indent = len(program_name) * ' '
        sys.stderr.write('{}: {}\n'.format(program_name, repr(e)))
        sys.stderr.write('{} for help use --help'.format(indent))
        return 2

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO,
                        format='%(levelname)s\t%(asctime)s\t%(name)s: %(message)s')
    if DEBUG:
        sys.argv.append('-h')
        sys.argv.append('-v')
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'vault_log_upload_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open('profile_stats.txt', 'wb')
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())
